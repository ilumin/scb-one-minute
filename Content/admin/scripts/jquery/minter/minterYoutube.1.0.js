(function( $ ) {
  
  var projectName = "minterYoutube";
  
  var setting = {
		width : 560,
		height : 315
  };
  var methods = {
	
	 init : function(){

		if(typeof arguments[0] != "string" ){
		  
		   return this;
		}	
		
		if(typeof arguments[1] == "object"){
			$.extend(setting,arguments[1]);
		}
		
		var youtubeID = arguments[0];
		
		return this.each(function(){
        
         var $this = $(this);
		 $this.html(_internal.getYoutubeIframe(youtubeID));
    
       });
		
	 }
  
  };
  
  var _internal = {
		getYoutubeIframe : function(youtubeID){
			var parameters = "wmode=opaque";
			
			if( typeof setting.autoplay != 'undefined' && setting.autoplay == 1){
			   parameters += "&autoplay=1";
			}
			
			if( typeof setting.controls != 'undefined'){
			   parameters += "&controls=" + setting.controls;
			}
			
			if( typeof setting.showinfo != 'undefined'){
			   parameters += "&showinfo=" + setting.showinfo;
			}
			
			
			return '<iframe width="'+ setting.width +'" height="'+ setting.height +'" src="http://www.youtube.com/embed/'+ youtubeID +'?'+ parameters +'" frameborder="0" allowfullscreen></iframe>';
		}
  };
  
  $.fn.minterYoutube = function(method) {
    
	if(!method){return this;}
  
	if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else {
      return methods.init.apply( this, arguments );
    }
  };
  
  
  
})( jQuery );