$(function(){

	var config = {
        toolbar : 
             [
	            { name: 'basicstyles', items : [ 'JustifyLeft','JustifyCenter','JustifyRight'] },
	            { name: 'basicstyles2', items : [ 'Bold','Italic','-','TextColor','BGColor' ] },
	            { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
	            { name: 'tools', items : [ 'Maximize','RemoveFormat','-','Source'] }
             ]
    };
	
	$('.frm_wysiwyg').ckeditor(config);
	
	$( "#tabs" ).tabs();
	
    $('#menu_control_Save').button({ 
		icons: {
        	primary: "ui-icon-disk"
    	}
	}).click(function(){
    	$('#wms_frm').submit();
    });
    
    $('#menu_control_Cancel').button({ 
		icons: {
        	primary: "ui-icon-closethick"
    	}
	});
    
    $( "#lang_frm" ).buttonset();
    
    $('#lang_en').click(function(){
    	
    	$('.lang_en').show();
    	$('.lang_th').hide();
    	
    });
    
    $('#lang_th').click(function(){
    	
    	$('.lang_th').show();
    	$('.lang_en').hide();
    	
    });
    
    //======= Tree option ======
    initOptionTree();
    
});

function initOptionTree(){
	//console.log("ok");
	var product_id = parseInt($('#product_id').val());
	var arr_select = [];
	//console.log(product_id);
	
	$.post(WEB_HOME + "product/initOptionTree", { id : product_id },
		   function(data){
				arr_select = data;
				
				var tree_options = {
				        empty_value: 'null',
				        indexed: true,  
				        on_each_change: WEB_HOME +'product/getSubCategory', 
				        choose: function(level) {
				            return 'Choose Category ';
				        },	       
				        preselect: {'cate_id':data.preSelect}
				        //loading_image : WEB_HOME + "image";
				      
				    };
				 $('#cate_id').optionTree(data.category, tree_options);
	}, "json");

}