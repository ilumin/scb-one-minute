var gridExtendOption = {
    colNames: ['ID', 'Title', 'Status', 'Created', ''],
    colModel: [
				{ name: 'id', index: 'id', width: 55 ,align:'right' },
				{ name: 'name', index: 'name', width: 400 },
				{ name: 'status', index: 'status', width:100 ,align:'center' },
				{ name: 'created_date', index: 'created_date',width:130 ,align:'center'},
				{ name: 'cmd', index: 'cmd', sortable: false ,width:40 ,align:'center'}
			],

    url: WEB_HOME + "menu_hilight/dataGrid",
    sortname: 'id',
    sortorder: 'desc',
    gridComplete: function() {

    var ids = $("#Grid").jqGrid('getDataIDs');
       
        for (var i = 0; i < ids.length; i++) {
            var cl = ids[i];
            be = "<a href='" + WEB_HOME + "menu_hilight/frm/"+ cl +"'>Edit</a>";
            $("#Grid").jqGrid('setRowData', ids[i], { cmd: be });
        }
    }

};

$(function(){
    $('#Grid').jqGrid($.extend(gridOptions,gridExtendOption)).jqGrid('sortableRows');
    
    $('#menu_control_New').button({ 
		icons: {
        	primary: "ui-icon-plusthick"
    	}
	});
    
    $('#menu_control_Save').button({ 
		icons: {
        	primary: "ui-icon-disk"
    	}
	}).click(function(){
    	
		var products = $("#Grid").getRowData();
		var dataIds = [];
		var dataOrdering = [];
		
		var data = [];
		
		$.each(products,function(index,value){
			dataIds[index] 		= value.id;
			dataOrdering[index] = value.ordering;

		});
				
		$.post(WEB_HOME + "menu_hilight/saveOrdering", { ids : dataIds , orderings : dataOrdering },
				   function(data){
			$("#Grid").trigger("reloadGrid");
			}, "json");
		
    });
    
});
