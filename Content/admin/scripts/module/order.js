var gridExtendOption = {
    colNames: ['ID','No', 'Firstname', 'Lastname', 'Tel','Email','Total','Created', ''],
    colModel: [
				{ name: 'id', index: 'id',search:false,hidden:true },
				{ name: 'order_no', index: 'order_no', width: 80,search:false,align:'right' },
				{ name: 'first_name', index: 'first_name',width:100 },
				{ name: 'last_name', index: 'last_name',width:100 },
				{ name: 'phoneno', index: 'phoneno' },
				{ name: 'email', index: 'email' },
				{ name: 'total_price', index: 'total_price',width:100,align:'right' },			
				{ name: 'created_date', index: 'created_date' },
				{ name: 'cmd', index: 'cmd', sortable: false ,search:false,width:80 }
			],

    url: WEB_HOME + "order/LoadGrid",
    sortname: 'order_no',
    sortorder: 'desc',
    //search : true,
    gridComplete: function() {

    var ids = $("#Grid").jqGrid('getDataIDs');
       
        for (var i = 0; i < ids.length; i++) {
            var cl = ids[i];
            be = "<a class='bt_view' href='" + WEB_HOME + "order/view/"+ cl +"'>View</a>";
            be += " <a class='bt_download'  href='" + WEB_HOME + "order/Download/"+ cl +"'>Export</a>";;
            $("#Grid").jqGrid('setRowData', ids[i], { cmd: be });
        }
        
        $('.bt_view').button({
	        icons: {
	        	primary: "ui-icon-document"
	    	}
        ,text : false
        });
        
        $('.bt_download').button({
	        icons: {
	        	primary: "ui-icon-arrowthickstop-1-s"
	    	}
        ,text : false
        });
    },
    
    afterInsertRow : function(rowid, rowdata)
    {
    	var col_name = rowdata.name;
    	var css = {};
    	
        if (rowdata.status == 'inactive'){
        	
        	col_name = ' <img width="15" src="'+ WEB_PATH +'images/status_icon/star_02.png" /> ' + col_name;
        	css = {color:'#666',background: 'url("") repeat scroll 0 0 #fff'};
            
        
        }else if(rowdata.status == 'active'){
        	col_name = ' <img width="15" src="'+ WEB_PATH +'images/status_icon/indicator_green.png" /> ' + col_name;
        }
        
        $(this).jqGrid('setRowData', rowid, {name: col_name}, css );
    }

};

$(function(){
    $('#Grid').jqGrid($.extend(gridOptions,gridExtendOption)).jqGrid('filterToolbar', { searchOnEnter: true, enableClear: false });
    
    $('#menu_control_New').button({ 
		icons: {
        	primary: "ui-icon-plusthick"
    	}
	});
    
   
    
});
