var gridExtendOption = {
    colNames: ['ID', 'Name','Category', 'Status', 'Created', 'Updated','Ordering', ''],
    colModel: [
				{ name: 'id', index: 'id', width: 55 ,align:'right' },
				{ name: 'name', index: 'name', width: 400 },
				{ name:'cate_id',index: 'cate_id', edittype: 'select', formatter: 'select', editoptions: { value: "1:โลโบ;2:ผงปรุงอาหารคาว;3:เครื่องปรุงอาหารคาว;4:ผงทำขนมหวาน;5:เครื่องแกง;6:น้ำจิ้ม;7:ผงปรุงสไตล์ตะวันออก;8:โลโบ ทูอินวัน;9:โลโบ เรดดี้"} },
				{ name: 'status', index: 'status', width:80 ,align:'center' },
				{ name: 'created_date', index: 'created_date',width:130 ,align:'center'},
				{ name: 'updated_date', index: 'updated_date',width:130 ,align:'center'},
				{ name: 'ordering', index: 'ordering', width: 55 ,align:'right' },
				{ name: 'cmd', index: 'cmd', sortable: false ,width:40 ,align:'center',search:false}
			],

    url: WEB_HOME + "product/dataGrid",
    sortname: 'ordering',
    sortorder: 'desc',
    gridModel : true,
    gridComplete: function() {

    var ids = $("#Grid").jqGrid('getDataIDs');
       
        for (var i = 0; i < ids.length; i++) {
            var cl = ids[i];
            be = "<a href='" + WEB_HOME + "product/frm/"+ cl +"'>Edit</a>";
            $("#Grid").jqGrid('setRowData', ids[i], { cmd: be });
            
            var grid_status = $("#Grid").jqGrid('getCell', ids[i],'status');
            if(grid_status == 'inactive'){
            	$("#Grid").jqGrid('setCell',ids[i],"status","",{color:'red'});
            }else if(grid_status == 'active'){
            	$("#Grid").jqGrid('setCell',ids[i],"status","",{color:'green'});
            }
        }
    }

};

$(function(){
    $('#Grid').jqGrid($.extend(gridOptions,gridExtendOption)).jqGrid('sortableRows');
    
    $('#menu_control_New').button({ 
		icons: {
        	primary: "ui-icon-plusthick"
    	}
	});
    
    $('#menu_control_Save').button({ 
		icons: {
        	primary: "ui-icon-disk"
    	}
	}).click(function(){
    	
		var products = $("#Grid").getRowData();
		var dataIds = [];
		var dataOrdering = [];
		
		var data = [];
		
		$.each(products,function(index,value){
			dataIds[index] 		= value.id;
			dataOrdering[index] = value.ordering;

		});
				
		$.post(WEB_HOME + "product/saveOrdering", { ids : dataIds , orderings : dataOrdering },
				   function(data){
			$("#Grid").trigger("reloadGrid");
			}, "json");
		
    });
    
    $('#search_cate_id, #search_status').change(customSearchGrid);
});

function customSearchGrid(){
	var cate_id = $('#search_cate_id').val();
	var status = $('#search_status').val();
	
	$("#Grid").jqGrid('setGridParam',{url:WEB_HOME + "product/dataGrid?cate_id="+cate_id+"&status="+status,page:1}).trigger("reloadGrid");
}

