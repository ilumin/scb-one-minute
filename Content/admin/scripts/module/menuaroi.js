var gridExtendOption = {
    colNames: ['ID', 'Name', 'Status', 'Created', 'Updated', ''],
    colModel: [
				{ name: 'id', index: 'id', width: 55 },
				{ name: 'name', index: 'name', width: 400 ,search:true},
				{ name: 'status', index: 'status' },
				{ name: 'created_date', index: 'created_date' },
				{ name: 'updated_date', index: 'updated_date' },
				{ name: 'cmd', index: 'cmd', sortable: false,search:false }
			],

    url: WEB_HOME + "menuaroi/menuaroiGrid",
    sortname: 'id',
    sortorder: 'desc',
    search : true,
    gridComplete: function() {

    var ids = $("#Grid").jqGrid('getDataIDs');
       
        for (var i = 0; i < ids.length; i++) {
            var cl = ids[i];
            be = "<a href='" + WEB_HOME + "menuaroi/create/"+ cl +"'>Edit</a>";
            $("#Grid").jqGrid('setRowData', ids[i], { cmd: be });
        }
    }

};

$(function(){
    $('#Grid').jqGrid($.extend(gridOptions,gridExtendOption)).jqGrid('filterToolbar', { searchOnEnter: true, enableClear: false });
    
    $('#menu_control_New').button({ 
		icons: {
        	primary: "ui-icon-plusthick"
    	}
	});
});
