$(function(){

	var config = {
        toolbar : 
             [
	            { name: 'basicstyles', items : [ 'JustifyLeft','JustifyCenter','JustifyRight'] },
	            { name: 'basicstyles2', items : [ 'Bold','Italic','-','TextColor','BGColor' ] },
	            { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
	            { name: 'tools', items : [ 'Maximize','RemoveFormat','-','Source'] }
             ]
    };
	
	$('.frm_wysiwyg').ckeditor(config);
	
	$( "#tabs" ).tabs();
	
    $('#menu_control_Save').button({ 
		icons: {
        	primary: "ui-icon-disk"
    	}
	}).click(function(){
    	$('#wms_frm').submit();
    });
    
    $('#menu_control_Cancel').button({ 
		icons: {
        	primary: "ui-icon-closethick"
    	}
	});
    
    $( "#lang_frm" ).buttonset();
    
    $('#lang_en').click(function(){
    	
    	$('.lang_en').show();
    	$('.lang_th').hide();
    	
    });
    
    $('#lang_th').click(function(){
    	
    	$('.lang_th').show();
    	$('.lang_en').hide();
    	
    });
    
    $('#ingredient_frm_th').sortable({
		placeholder: "ui-state-highlight"
	});
    
    $('#add_ingredient_th').button({ 
		icons: {
        	primary: "ui-icon-plus"
    	}
	}).click(menuaroi_command.addIngredientTh);
    
    $('#add_ingredient_en').button({ 
		icons: {
        	primary: "ui-icon-plus"
    	}
	}).click(menuaroi_command.addIngredientEn);
    
    $('.remove_ingredient').live('click',function(){
    	
    	$(this).parent().slideUp(200,function(){
    		$(this).remove();
    	});
    	
    });
    
    $('#youtube_id').change(function(){
    	if($('#youtube_id').val() != ''){
        	$('#youtube_show').minterYoutube($('#youtube_id').val(),{width:500,height:304,autoplay:0,showinfo:0});
        }
    });
    if($('#youtube_id').val() != ''){
    	$('#youtube_show').minterYoutube($('#youtube_id').val(),{width:500,height:304,autoplay:0,showinfo:0});
    }
    
});

var menuaroi_command = {
		
		addIngredientTh : function(){
			$('#ingredient_frm_th').append(
				$('<li>').append(
			      $('<input>').attr({type:'text',name:'ingredient_1_th[]'})
				).append(
			      $('<input>').attr({type:'text',name:'ingredient_2_th[]'})
				).append(
			      $('<input>').attr({type:'text',name:'ingredient_3_th[]'})
				).append(
			      $('<div>').addClass('remove_ingredient ui-state-default ui-corner-all').append(
			    	 $('<span>').addClass('ui-icon ui-icon-minus')
			      )
				)
			);
		},
		
		addIngredientEn : function(){
			$('#ingredient_frm_en').append(
				$('<li>').append(
			      $('<input>').attr({type:'text',name:'ingredient_1_en[]'})
				).append(
			      $('<input>').attr({type:'text',name:'ingredient_2_en[]'})
				).append(
			      $('<input>').attr({type:'text',name:'ingredient_3_en[]'})
				).append(
			      $('<div>').addClass('remove_ingredient ui-state-default ui-corner-all').append(
			    	 $('<span>').addClass('ui-icon ui-icon-minus')
			      )
				)
			);
		}
		
};
