var gridExtendOption = {
    colNames: ['ID', 'Title', 'Status','Publish', 'Created', 'Updated', ''],
    colModel: [
				{ name: 'id', index: 'id', width: 55 ,align:'right' },
				{ name: 'name', index: 'name', width: 400 },
				{ name: 'status', index: 'status', width:80 ,align:'center' },
				{ name: 'publish_date', index: 'publish_date',width:130 ,align:'center' },
				{ name: 'created_date', index: 'created_date',width:130 ,align:'center'},
				{ name: 'updated_date', index: 'updated_date',width:130 ,align:'center'},
				{ name: 'cmd', index: 'cmd', sortable: false ,width:40 ,align:'center'}
			],

    url: WEB_HOME + "newsevent/dataGrid",
    sortname: 'id',
    sortorder: 'desc',
    gridComplete: function() {

    var ids = $("#Grid").jqGrid('getDataIDs');
       
        for (var i = 0; i < ids.length; i++) {
            var cl = ids[i];
            be = "<a href='" + WEB_HOME + "newsevent/frm/"+ cl +"'>Edit</a>";
            $("#Grid").jqGrid('setRowData', ids[i], { cmd: be });
        }
    }

};

$(function(){
    $('#Grid').jqGrid($.extend(gridOptions,gridExtendOption));
    
    $('#menu_control_New').button({ 
		icons: {
        	primary: "ui-icon-plusthick"
    	}
	});
});
