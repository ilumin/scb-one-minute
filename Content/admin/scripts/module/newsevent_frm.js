$(function(){

	var config = {
        toolbar : 
             [
	            { name: 'basicstyles', items : [ 'JustifyLeft','JustifyCenter','JustifyRight'] },
	            { name: 'basicstyles2', items : [ 'Bold','Italic','-','TextColor','BGColor' ] },
	            { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
	            { name: 'tools', items : [ 'Maximize','RemoveFormat','-','Source'] }
             ]
    };
	
	$('.frm_wysiwyg').ckeditor(config);
	
	$( "#tabs" ).tabs();
	
    $('#menu_control_Save').button({ 
		icons: {
        	primary: "ui-icon-disk"
    	}
	}).click(function(){
    	$('#wms_frm').submit();
    });
    
    $('#menu_control_Cancel').button({ 
		icons: {
        	primary: "ui-icon-closethick"
    	}
	});
    
    $( "#lang_frm" ).buttonset();
    
    $('#lang_en').click(function(){
    	
    	$('.lang_en').show();
    	$('.lang_th').hide();
    	
    });
    
    $('#lang_th').click(function(){
    	
    	$('.lang_th').show();
    	$('.lang_en').hide();
    	
    });
    
   $('#publish_date').datepicker();
    
});
