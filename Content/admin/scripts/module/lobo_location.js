var gridExtendOption = {
    colNames: ['ID', 'Name', 'Status', 'Created', 'Updated', ''],
    colModel: [
				{ name: 'id', index: 'id', width: 55 ,search:false},
				{ name: 'name', index: 'name', width: 400 ,search:true},
				{ name: 'status', index: 'status' ,search:false},
				{ name: 'created_date', index: 'created_date' ,search:false},
				{ name: 'updated_date', index: 'updated_date',search:false },
				{ name: 'cmd', index: 'cmd', sortable: false,search:false }
			],

    url: WEB_HOME + "lobo_location/dataGrid",
    sortname: 'id',
    sortorder: 'desc',
    search : true,
    gridComplete: function() {

    var ids = $("#Grid").jqGrid('getDataIDs');
       
        for (var i = 0; i < ids.length; i++) {
            var cl = ids[i];
            be = "<a href='" + WEB_HOME + "lobo_location/create/"+ cl +"'>Edit</a>";
            $("#Grid").jqGrid('setRowData', ids[i], { cmd: be });
        }
    }
};

$(function(){
    $('#Grid').jqGrid($.extend(gridOptions,gridExtendOption)).jqGrid('filterToolbar', { searchOnEnter: true, enableClear: false });
    
    $('#menu_control_New').button({ 
		icons: {
        	primary: "ui-icon-plusthick"
    	}
	});
	
	$('#provinceFilter').change(function(){
		var province_id = $(this).val();	
		$("#Grid").jqGrid('setGridParam',{url:WEB_HOME + "lobo_location/dataGrid?province_id="+province_id,page:1}).trigger("reloadGrid");
		
		$.post(WEB_HOME + "lobo_location/getAmphur",{province_id : province_id},function(data){
			
			var element = '<option value="0">ทั้งหมด / All</option>';
			
			$.each(data,function(i,val){
				element += '<option value="'+ val.amphur_id +'">' + val.amphur_name_th + ' / ' + val.amphur_name_en + '</option>';
			});
			
			$('#amphurFilter').html(element);
			
		},'json');
		
	});
	
	$('#amphurFilter').change(function(){
		var amphur_id = $(this).val();
		$("#Grid").jqGrid('setGridParam',{url:WEB_HOME + "lobo_location/dataGrid?amphur_id="+amphur_id,page:1}).trigger("reloadGrid");
	});
	
});