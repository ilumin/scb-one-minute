$(function(){

	var config = {
        toolbar : 
             [
	            { name: 'basicstyles', items : [ 'JustifyLeft','JustifyCenter','JustifyRight'] },
	            { name: 'basicstyles2', items : [ 'Bold','Italic','-','TextColor','BGColor' ] },
	            { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
	            { name: 'tools', items : [ 'Maximize','RemoveFormat','-','Source'] }
             ]
    };
	
	$('.frm_wysiwyg').ckeditor(config);
	
	$( "#tabs" ).tabs();
	
    $('#menu_control_Save').button({ 
		icons: {
        	primary: "ui-icon-disk"
    	}
	}).click(function(){
    	$('#wms_frm').submit();
    });
    
    $('#menu_control_Cancel').button({ 
		icons: {
        	primary: "ui-icon-closethick"
    	}
	});
    
    $( "#lang_frm" ).buttonset();
    
    $('#lang_en').click(function(){
    	
    	$('.lang_en').show();
    	$('.lang_th').hide();
    	
    });
    
    $('#lang_th').click(function(){
    	
    	$('.lang_th').show();
    	$('.lang_en').hide();
    	
    });
    
   $('#province_id').change(function(){
   		
		$.post(WEB_HOME + "lobo_location/getAmphur",{province_id : $(this).val()},function(data){
			
			var element;
			
			$.each(data,function(i,val){
				element += '<option value="'+ val.amphur_id +'">' + val.amphur_name_th + ' / ' + val.amphur_name_en + '</option>';
			});
			
			$('#amphur_id').html(element);
			
		},'json');
	
   });
    
	$('#cateFilterMenu').click(function(e){
		e.preventDefault();
		var $span = $(this).find('span');
		if($span.hasClass('ui-icon-minus')){
			$span.removeClass('ui-icon-minus').addClass('ui-icon-plus');
			$('#cateFilterElement').hide();
		}else{
			$span.removeClass('ui-icon-plus').addClass('ui-icon-minus');
			$('#cateFilterElement').show();
		}
		
	});
	
	
	$('.category_filter').click(function(){
		
		if($(this).hasClass('hasCateChild')){
			
			var parent_id = $(this).data('id');
			
			if($(this).attr('checked')){
				$('.parentId_' + parent_id).attr('checked',true);
			}else{
				$('.parentId_' + parent_id).attr('checked',false);
			}
	
		}
		
		$('#relateProductList li').hide();		
		$('.category_filter:checked').each(function(){
			
			var id = $(this).data('id');
			$('.product_cate_'+id).show();
			
		});
		
	});
});


