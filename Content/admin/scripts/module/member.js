var gridExtendOption = {
    colNames: ['ID', 'Firstname', 'Lastname', 'Tel','Mobile','Email','Created Date', ''],
    colModel: [
				{ name: 'id', index: 'id', width: 55,search:false },
				{ name: 'first_name', index: 'first_name' },
				{ name: 'last_name', index: 'last_name' },
				{ name: 'tel_no', index: 'tel_no' },
				{ name: 'mobile_no', index: 'mobile_no' },
				{ name: 'email', index: 'email' },
				{ name: 'timestamp', index: 'timestamp' },
				{ name: 'cmd', index: 'cmd', sortable: false ,search:false }
			],

    url: WEB_HOME + "member/LoadGrid",
    sortname: 'id',
    sortorder: 'desc',
  //  search : true,
    gridComplete: function() {

    var ids = $("#Grid").jqGrid('getDataIDs');
       
        for (var i = 0; i < ids.length; i++) {
            var cl = ids[i];
            be = "<a class='bt_view' href='" + WEB_HOME + "member/view/"+ cl +"'>View</a>";
            $("#Grid").jqGrid('setRowData', ids[i], { cmd: be });
        }
        
        $('.bt_view').button({
	        icons: {
	        	primary: "ui-icon-document"
	    	}
        ,text : false
        });
        
    },
    
    afterInsertRow : function(rowid, rowdata)
    {
    	var col_name = rowdata.name;
    	var css = {};
    	
        if (rowdata.status == 'inactive'){
        	
        	col_name = ' <img width="15" src="'+ WEB_PATH +'images/status_icon/star_02.png" /> ' + col_name;
        	css = {color:'#666',background: 'url("") repeat scroll 0 0 #fff'};
            
        
        }else if(rowdata.status == 'active'){
        	col_name = ' <img width="15" src="'+ WEB_PATH +'images/status_icon/indicator_green.png" /> ' + col_name;
        }
        
        $(this).jqGrid('setRowData', rowid, {name: col_name}, css );
    }

};

$(function(){
    $('#Grid').jqGrid($.extend(gridOptions,gridExtendOption)).jqGrid('filterToolbar', { searchOnEnter: true, enableClear: false });
    
    $('#menu_control_Export').button({ 
		icons: {
        	primary: "ui-icon-arrowthickstop-1-s"
    	}
	});
});
