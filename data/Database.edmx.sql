
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 09/22/2014 14:44:32
-- Generated from EDMX file: C:\Projects\review\one minute\source\Website\Models\Database.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [one_minute];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Account]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Account];
GO
IF OBJECT_ID(N'[dbo].[Log]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Log];
GO
IF OBJECT_ID(N'[dbo].[SelectTime]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SelectTime];
GO
IF OBJECT_ID(N'[dbo].[Invite]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Invite];
GO
IF OBJECT_ID(N'[dbo].[Gallery]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Gallery];
GO
IF OBJECT_ID(N'[dbo].[VDOBackground]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VDOBackground];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Account'
CREATE TABLE [dbo].[Account] (
    [AccountID] int IDENTITY(1,1) NOT NULL,
    [Type] int  NOT NULL,
    [Email] nvarchar(255)  NULL,
    [Username] nvarchar(100)  NULL,
    [Password] nvarchar(50)  NULL,
    [Firstname] nvarchar(255)  NULL,
    [Lastname] nvarchar(255)  NULL,
    [Telephone] nvarchar(50)  NULL,
    [IsActive] bit  NULL,
    [ActivateCode] nvarchar(50)  NULL,
    [ActiveDate] datetime  NULL,
    [FBUID] bigint  NULL,
    [FBEmail] nvarchar(1000)  NULL,
    [FBAccessToken] varchar(1000)  NULL,
    [FBSessionKey] nvarchar(255)  NULL,
    [FBFirstname] nvarchar(255)  NULL,
    [FBLastname] nvarchar(255)  NULL,
    [FBPicSquare] nvarchar(1000)  NULL,
    [FBFullname] nvarchar(1000)  NULL,
    [FBSex] nvarchar(10)  NULL,
    [FBLocale] nvarchar(255)  NULL,
    [FBTimezone] int  NULL,
    [CreateDate] datetime  NOT NULL,
    [UpdateDate] datetime  NOT NULL,
    [AlertInviteDate] datetime  NULL
);
GO

-- Creating table 'Log'
CREATE TABLE [dbo].[Log] (
    [LogID] int IDENTITY(1,1) NOT NULL,
    [ObjectID] int  NULL,
    [LogType] nvarchar(max)  NULL,
    [LogName] nvarchar(max)  NULL,
    [Value1] nvarchar(max)  NULL,
    [Value2] nvarchar(max)  NULL,
    [Value3] nvarchar(max)  NULL,
    [Value4] nvarchar(max)  NULL,
    [Value5] nvarchar(max)  NULL,
    [Value6] nvarchar(max)  NULL,
    [Value7] nvarchar(max)  NULL,
    [Value8] nvarchar(max)  NULL,
    [Value9] nvarchar(max)  NULL,
    [Value10] nvarchar(max)  NULL,
    [CreateDate] datetime  NULL,
    [IPAddress] varchar(50)  NULL,
    [Browser] nvarchar(1000)  NULL,
    [CookiesEnable] bit  NULL,
    [JavascriptVersion] nvarchar(100)  NULL,
    [BrowserMajorVersion] nvarchar(100)  NULL,
    [BrowserMinorVersion] nvarchar(100)  NULL,
    [BrowserPlatform] nvarchar(100)  NULL,
    [BrowserType] nvarchar(100)  NULL,
    [VBScriptEnable] bit  NULL,
    [CLRVersion] nvarchar(100)  NULL,
    [FlashVersion] nvarchar(100)  NULL,
    [FlashOS] nvarchar(100)  NULL,
    [FlashVideoEnable] bit  NULL,
    [FlashAudioEnable] bit  NULL,
    [FlashLocalFileEnable] bit  NULL
);
GO

-- Creating table 'SelectTime'
CREATE TABLE [dbo].[SelectTime] (
    [SelectTimeID] int IDENTITY(1,1) NOT NULL,
    [AccountID] int  NOT NULL,
    [Time] datetime  NOT NULL,
    [CreateDate] datetime  NOT NULL
);
GO

-- Creating table 'Invite'
CREATE TABLE [dbo].[Invite] (
    [InviteID] int IDENTITY(1,1) NOT NULL,
    [Type] int  NOT NULL,
    [AccountID] int  NOT NULL,
    [GUID] nvarchar(max)  NULL,
    [FBRequestID] bigint  NULL,
    [FBUserID] bigint  NULL,
    [Email] nvarchar(max)  NULL,
    [IsAccept] bit  NOT NULL,
    [IsJoin] bit  NOT NULL,
    [AcceptAccountID] int  NULL,
    [CreateDate] datetime  NOT NULL,
    [AcceptDate] datetime  NULL
);
GO

-- Creating table 'Gallery'
CREATE TABLE [dbo].[Gallery] (
    [GalleryID] int IDENTITY(1,1) NOT NULL,
    [Thumb] nvarchar(max)  NOT NULL,
    [VDOURL] nvarchar(max)  NOT NULL,
    [EventDate] datetime  NOT NULL,
    [CreateDate] datetime  NOT NULL
);
GO

-- Creating table 'VDOBackground'
CREATE TABLE [dbo].[VDOBackground] (
    [VDOBackgroundID] int IDENTITY(1,1) NOT NULL,
    [VDOID] nvarchar(20)  NOT NULL,
    [StartTime] time  NOT NULL,
    [EndTime] time  NOT NULL,
    [CreateDate] datetime  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AccountID] in table 'Account'
ALTER TABLE [dbo].[Account]
ADD CONSTRAINT [PK_Account]
    PRIMARY KEY CLUSTERED ([AccountID] ASC);
GO

-- Creating primary key on [LogID] in table 'Log'
ALTER TABLE [dbo].[Log]
ADD CONSTRAINT [PK_Log]
    PRIMARY KEY CLUSTERED ([LogID] ASC);
GO

-- Creating primary key on [SelectTimeID] in table 'SelectTime'
ALTER TABLE [dbo].[SelectTime]
ADD CONSTRAINT [PK_SelectTime]
    PRIMARY KEY CLUSTERED ([SelectTimeID] ASC);
GO

-- Creating primary key on [InviteID] in table 'Invite'
ALTER TABLE [dbo].[Invite]
ADD CONSTRAINT [PK_Invite]
    PRIMARY KEY CLUSTERED ([InviteID] ASC);
GO

-- Creating primary key on [GalleryID] in table 'Gallery'
ALTER TABLE [dbo].[Gallery]
ADD CONSTRAINT [PK_Gallery]
    PRIMARY KEY CLUSTERED ([GalleryID] ASC);
GO

-- Creating primary key on [VDOBackgroundID] in table 'VDOBackground'
ALTER TABLE [dbo].[VDOBackground]
ADD CONSTRAINT [PK_VDOBackground]
    PRIMARY KEY CLUSTERED ([VDOBackgroundID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------