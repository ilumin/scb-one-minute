var __vexContent;
var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
// var months = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
var mousewheelevt = (/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel";
var mobile_ui = $('.mobile-ui').length > 0;

jQuery.extend(jQuery.fn.pickadate.defaults,{monthsFull:["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"],monthsShort:["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."],weekdaysFull:["อาทติย","จันทร","องัคาร","พุธ","พฤหสั บดี","ศกุร","เสาร"],weekdaysShort:["อ.","จ.","อ.","พ.","พฤ.","ศ.","ส."],today:"วันนี้",clear:"ลบ",format:"d mmmm yyyy",formatSubmit:"yyyy/mm/dd"});

var w = 630
  , h = 630
  , strokeColor = "#ffc000"

  // variable for arc
  , arcStartAngle = 0
  , arcInnerRadius = w / 2 - 20
  , arcOuterRadius = w / 2 - 10

  // variable for circle
  , circleRadius = 10
  , anglePoint = 0

  // variable for animate arc path
  , fullAngle = Math.PI * 2
  , maxStep = 24 * 60
  , angleStep = fullAngle / maxStep
  , currentStep = 0
  , animateDuration = 50

  , svg, arcGroup, arc, arcPath, circle
  ;

String.prototype.padLeft = function (length, character) { 
  return new Array(length - this.length + 1).join(character || '0') + this; 
}

function toggleMenu() {
    var menu = $('.nav-top');

    if (menu.hasClass('active')) {
        menu.removeClass('active');
        console.log($('.mobile-ui'));
        $('.mobile-ui').height('100%');
    }
    else {
        menu.addClass('active');
        $('.mobile-ui').height($('.nav ul').height() + 64);
    }
}

function renderSmallClock(id, width, height, start, end, direction) {
  var _arcStartAngle = 0
    , _arcInnerRadius = width / 2 - 13
    , _arcOuterRadius = width / 2 - 10
    , _circleRadius = 10
    , _animateDuration = 600
    , fullAngle = Math.PI * 2
    , maxStep = 360
    , _angleStep = fullAngle / maxStep
    ;

  var _svg = d3.select("#" + id).append("svg:svg")
    .attr("width", width)
    .attr("height", height)
    .attr("id", "clock")
    ;

  // _svg
  //   .append('defs')
  //   .append('pattern')
  //   .attr('id', 'sclock')
  //   .append('image')
  //   .attr('xlink:href', 'assets/images/icon-small-clock.png');

  // create arc group
  var _arcGroup = _svg.append("svg:g")
    .attr(
      "transform", 
      "translate(" + width / 2 + "," + height / 2 + ")"
    )
    ;

  var _arc = d3.svg.arc()
    .innerRadius( _arcInnerRadius )
    .outerRadius( _arcOuterRadius )
    .startAngle( start )
    ;

  var _arcPath = _arcGroup.append("path")
    .datum({ endAngle: start })
    .style("fill", "#FFFFFF")
    .attr("d", _arc)
    ;

  var _circle = _arcGroup.append("circle")
    .attr("r", _circleRadius)
    .attr("fill", "url(#icon)")
    ;

  var _arcTween = function(transition, newAngle, arc) {
    transition.attrTween("d", function(d) {
      var interpolate = d3.interpolate(d.endAngle, newAngle);
      return function(t) {
        d.endAngle = interpolate(t) * direction;
        
        anglePoint = Math.ceil(d.endAngle / _angleStep);
        
        _moveCircle(anglePoint);
        
        return arc(d);
      };
    });
  }

  var _moveCircle = function(angle) {
    var r = height/2 - 12;
    var x = r * Math.sin(angle * Math.PI / 180);
    var y = -r * Math.cos(angle * Math.PI / 180);
    
    _circle 
      .attr("cx", x)
      .attr("cy", y)
      ;
  }

  var _setAngleStep = function(step) {
    if (step > maxStep || step < 0)
      return;

    _arcPath.transition()
      .duration(_animateDuration)
      .ease("linear")
      .call(
        _arcTween,
        _angleStep * step, 
        _arc
      );
  }
  
  _setAngleStep(end);
 }

function renderClock(renderTime) {
  // create main svg
  svg = d3.select("#svg").append("svg:svg")
    .attr("width", w)
    .attr("height", h)
    .attr("id", "clock")
    ;

  // create arc group
  arcGroup = svg.append("svg:g")
    .attr(
      "transform", 
      "translate(" + w / 2 + "," + h / 2 + ")"
    )
    ;

  // create arc object (not display in svg,
  // but use as data of path)
  arc = d3.svg.arc()
    .innerRadius( arcInnerRadius )
    .outerRadius( arcOuterRadius )
    .startAngle( arcStartAngle )
    ;

  // create arc path
  arcPath = arcGroup.append("path")
    .datum({ endAngle: arcStartAngle })
    .style("fill", strokeColor)
    .attr("d", arc)
    ;

  circle = arcGroup.append("circle")
    .attr("r", circleRadius)
    .attr("fill", "#ffffff")
    .attr("stroke", strokeColor)
    .attr("stroke-width", circleRadius/2 + 2)
    .attr("cursor", "move")

    // .attr("transform", "translate(0,-" + (h/2 - circleRadius - 5) + ")")
    .call( d3.behavior.drag().on('drag', function(){
      a = findAngle(d3.event.x, d3.event.y);
      
      currentStep = angleToStep(a);
      setAngleStep(currentStep);
    }) )
    ;

  // define current step
  if (renderTime) {
    var __time = renderTime.split(':');
    currentStep = parseInt(__time[0] * 60) + parseInt(__time[1]);
  }

  // init
  setAngleStep(currentStep);

  var svgDOM = document.getElementById('svg');
  if (svgDOM.attachEvent)
    svgDOM.attachEvent("on"+mousewheelevt, function(e){ delegateMousewheel(e); });
  else if (svgDOM.addEventListener)
    svgDOM.addEventListener(mousewheelevt, function(e){ delegateMousewheel(e); }, false);
}

// set animate step
function setAngleStep(step) {
  if (step > maxStep || step < 0)
    return;
  
  arcPath.transition()
    .duration(animateDuration)
    .ease("linear")
    .call(
      arcTween,
      angleStep * step, 
      arc
    );

  updateClockTime( step );

  return step;
}

// animate function
function arcTween(transition, newAngle, arc) {
  // arc path transition
  transition.attrTween("d", function(d) {
    var interpolate = d3.interpolate(d.endAngle, newAngle);
    return function(t) {
      d.endAngle = interpolate(t);
      
      // transalte circle
      anglePoint = Math.ceil(d.endAngle / angleStep) / 4;
      
      moveCircle(anglePoint);
      
      return arc(d);
    };
  });
}

function moveCircle(angle) {
  var r = h/2 - 15;
  var x = r * Math.sin(angle * Math.PI / 180);
  var y = -r * Math.cos(angle * Math.PI / 180);
  
  // circle.attr(
  //   "transform", 
  //   "translate(" + x + "," + y + ")"
  // );
  circle 
    .attr("cx", x)
    .attr("cy", y)
    ;
}

function angleToStep(angle) {
  return Math.ceil( angle * 4 );
}

function findAngle(x, y) {
  addAngle = x < 0 ? 270 : 90;
  return (Math.atan(y/x) * 180 / Math.PI) + addAngle; 
}

function tinyScrollTo(id) {
    var _all_event = $('.event-item').length;
    var itemIndex = $('#' + id).index('.event-item');
    var scroll = itemIndex * 191;

    // Animate it
    $('.overview:first').animate({
        top: -scroll
    }, 600, function () {
        // Animation complete.
    });

    trHeight = $('.track:first').height();
    tScroll = (trHeight / _all_event) * itemIndex;

    $('.thumb').animate({
        top: tScroll
    }, 600, function () {
        // Animation complete.
    });
}

function open_invite() {
  modal_open('#modal-invite', function(vexContent) {
      vexContent.addClass('invite-modal');

      var $frm = vexContent.find('form');

      $frm.validate({
          rules: {
              email1: { required: true, email: true },
              email2: { email: true },
              email3: { email: true }
          },
          messages: {
              email1: { required: '*', email: '*' },
              email2: { email: '*' },
              email3: { email: '*' }
          }
      });
      /*$frm.find('input[type="text"]').each(function () {
          $(this).rules("add", {
              email: true,
              messages: { email: '*' }
          });
      });

      $frm.find('#txt-email1').rules("add", {
          required: true,
          messages: { required: '*' }
      });*/

      //var actionURL = $frm.attr('action');
      $frm.ajaxForm({
          //url: actionURL,
          type: 'post',
          dataType: 'json',
          beforSubmit: function (rs) { vex.close(); },
          success: function (rs) { vex.close(); },
          error: function (er) { open_alert('error'); }
      });
  });
}

function open_register(firstname, lastname, email) {
    modal_open('#modal-register', function(vexContent) {
        $form = vexContent.find('#frm-register');

        if (firstname) $form.find('input[name="firstname"]').val(firstname);
        if (lastname) $form.find('input[name="lastname"]').val(lastname);
        if (email) $form.find('input[name="email"]').val(email);

        $form.validate({
            rules: {
                username: { required: true },
                password: { required: true },
                firstname: { required: true },
                lastname: { required: true },
                telephone: { required: true },
                email: { required: true, email: true }
            },
            messages: {
                username: { required: '*' },
                password: { required: '*' },
                firstname: { required: '*' },
                lastname: { required: '*' },
                telephone: { required: '*' },
                email: { required: '*', email: '*' },
            }
        });

        $form.ajaxForm({
            url: $form.attr('action'),
            type: 'post',
            dataType: 'json',
            success: function (rs) {
                if (rs.message == 'true') {
                    open_selectDate();
                } else if (rs.message == 'false') {
                    $form.find('.error-message').text(rs.result);
                }
            }
        });
    });
}

function open_login_facebook() {
  modal_open('#modal-login', function(vexContent) {
    vexContent.addClass('facebook-modal');
  });
}

function open_login() {
    modal_open('#modal-login', function (vexContent) {
        $form = vexContent.find('#frm-login');
        $form.validate({
            rules: {
                username: { required: true },
                password: { required: true }
            },
            messages: {
                username: { required: '*' },
                password: { required: '*' }
            }
        });

        $form.ajaxForm({
            url: $form.attr('action'),
            type: 'post',
            dataType: 'json',
            success: function (rs) {
                if (rs.message == 'true') {
                    window.location.reload();
                } else if (rs.message == 'false') {
                    open_alert(rs.result);
                }
            }
        });
    });
}

function open_thankyou() {
  modal_open('#modal-thankyou');
}

function open_alert(msg) {
  modal_open('#modal-error', function(vexContent) {
    vexContent
      .addClass('error-modal')
      .find('.context .message').html(msg).end()
      ;
  });
}

function error_time() {
  open_alert('เวลาที่คุณเลือก ไม่สามารถลงได้ กรุณาเลือกเวลาใหม่');
}

function open_player(vdoPath, displayTime, callback, vdoid, baseURL, img) {
  modal_open('#modal-vdo-player', function(vexContent) {
    vdoPath = '//www.youtube.com/embed/' + vdoPath + '?controls=0&loop=0&modestbranding=0&showinfo=0&enablejsapi=1&vq=hd720';
    
    vexContent
      .addClass('transparent vdo-modal')
      .find('#youtube').attr('src', vdoPath).end()
      .find('.time').html(displayTime);

    vexContent.find('.button-share').unbind('click').click(function () {
        var obj = {
            method: 'feed',
            link: baseURL + 'gallery#' + vdoid,
            picture: baseURL + img,
            name: 'SCB One Minute ชีวิตคิดไม่ถึง',
            caption: baseURL,
            description: 'ลุ้นรับรางวัลกับเหตุไม่คาดฝัน เพียงเข้ามาเลือก 1 นาที ในแต่ละวันคลิกร่วมสนุกเลย'
        };

        FB.ui(obj);
    });

    play_vdo('youtube');

    callback && callback(vexContent);
  });
}

function open_winner(vdoPath, displayTime, displayDate, winnerList, vdoid, baseURL, img) {
  open_player(vdoPath, displayTime, function(vexContent) {
    var winnerHtml = $('#modal-winner').html(),
      winnerListHtml = '';

    winnerHtml = $('<div>').html( winnerHtml );

    winnerHtml.find('h3 strong').html(displayDate);

    for (var i = 0, j = winnerList.length; i < j; i++ ) {
      winnerListHtml += '<li><img src="' + winnerList[i]['avatar'] + '"><strong class="name">' + winnerList[i]['name'] + '</strong></li>';
    }

    winnerHtml.find('.winner-list').html(winnerListHtml);

    vexContent
      .addClass('winner-modal')
      .append(winnerHtml.html());

    /*vexContent.unbind('click').find('.button-share').click(function () {
        var obj = {
            method: 'feed',
            link: baseURL + 'gallery#' + vdoid,
            picture: baseURL + img,
            name: 'SCB One Minute ชีวิตคิดไม่ถึง',
            caption: baseURL,
            description: 'ลุ้นรับรางวัลกับเหตุไม่คาดฝัน เพียงเข้ามาเลือก 1 นาที ในแต่ละวันคลิกร่วมสนุกเลย'
        };

        FB.ui(obj);
    });*/
  }, vdoid, baseURL, img);
}

function open_confirm(avatarPath, selectedTime, date) {
  modal_open('#modal-confirm', function(vexContent) {
    vexContent
      .find('.time').html(selectedTime).end()
      .find('.avatar').attr('src', avatarPath);

    vexContent.find('input[name="date_submit"]').val(date);
    vexContent.find('input[name="time_submit"]').val(selectedTime.replace(':', ''));
    vexContent.find('.button-confirm').click(function () {
        vexContent.find('form').submit();
    })
    vexContent.find('.button-reset').click(function () {
        if ($('a.dummy').length > 0) 
            open_selectDate();
        else
            vex.close();
    });
  });
}

function open_selectDate(date) {
    modal_open('#modal-select-date', function (vexContent) {
        var current_date = new Date();
        var dat = new Date();
        current_date.setDate(dat.getDate() + 1);
        if (typeof (date) != 'undefined') current_date = date;

        vexContent
          .addClass('transparent choose-date');

        vexContent.find('.datepicker')
          .pickadate({
              min: 1,
              max: [2014, 9, 28],
              format: 'dd mmm',
              formatSubmit: 'ddmm',
              monthsShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
              onSet: function (context) {
                  //console.log('Set stuff:', context.select);
              },
              onStart: function () {
                  this.set('select', current_date)
              }
          })
          //.val(current_date.getDate() + ' ' + jQuery.fn.pickadate.defaults.monthsShort[current_date.getMonth()])
          ;

        vexContent.find('.timepicker')
          .val((current_date.getHours()).toString().toString().padLeft(2) + ':' + (current_date.getMinutes()).toString().padLeft(2))
          .pickatime({
              format: 'HH:i',
              formatSubmit: 'HHi',
              onSet: function (context) {
                  currentStep = context.select;
                  setAngleStep(context.select);
                  //console.log('Set stuff:', context.select)
              }
          });

        if (!mobile_ui)
            renderClock(current_date.getHours() + ':' + current_date.getMinutes());

        var $formSelect = vexContent.find('#form-select');
        $formSelect.ajaxForm({
            url: $formSelect.attr('action'),
            type: 'post',
            dataType: 'json',
            success: function (rs) {
                if (rs.message == 'true') {
                    /*var $formPost = vexContent.find('#form-post');
                    $formPost.find('input[name="time"]').val($formSelect.find('input[name="time_submit"]').val());
                    $formPost.find('input[name="date"]').val($formSelect.find('input[name="date_submit"]').val());
                    $formPost.submit();*/
                    open_confirm(userpic, $formSelect.find('input[name="time"]').val(), $formSelect.find('input[name="date_submit"]').val());
                } else if (rs.result == 'not login') {
                    open_login();
                } else if (rs.result == 'timelimit') {
                    open_alert('วันนี้คุณได้ใช้สิทธิ์ลงเวลาไปครบแล้ว กรุณากลับมาร่วมสนุกได้อีกครั้งในวันพรุ่งนี้');
                } else if (rs.result == 'timeerror') {
                    open_alert('คุณไม่สามารถเลือกเวลาซ้ำ กรุณาเลือกเวลาใหม่');
                } else {
                    error_time();
                }
            },
            error: function (er) { }
        });
    });
}

function open_confirmpass(data) {
    modal_open('#modal-confirmpass', function (vexContent) {
        vexContent.css({ height: 195 })
        .find('span').text(data.email);

        $form = vexContent.find('form');
        $form.validate({
            rules: {
                password: { required: true }
            },
            messages: {
                password: { required: '*' }
            }
        });

        $form.ajaxForm({
            url: $form.attr('action'),
            type: 'post',
            dataType: 'json',
            data: data,
            success: function (rs) {
                if (rs.message == 'true') {
                    window.location.reload();
                } else if (rs.message == 'false') {
                    open_alert(rs.result);
                }
            }
        });
    });
}

function modal_open_html(data, afterOpen, afterClose) {
  // close current modal
  if (__vexContent) {
    __vexContent.data().vex && vex.close(__vexContent.data().vex.id);
  }

  // open modal
  __vexContent = vex.open({
    content: data,
    buttons: false,
    showCloseButton: false,
    afterOpen: afterOpen,
    afterClose: afterClose
  });
}

function modal_open(selector, afterOpen, afterClose) {
  // vex exist ?
  if (typeof(vex)=='undefined') return;

  // content exist ?
  var modal_content = $(selector);
  if (typeof(modal_content)=='undefined') return;

  modal_open_html(modal_content.html(), afterOpen, afterClose);
}

function callPlayer(frame_id, func, args) {
  if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;
  var iframe = document.getElementById(frame_id);
  if (iframe && iframe.tagName.toUpperCase() != 'IFRAME') {
    iframe = iframe.getElementsByTagName('iframe')[0];
  }

  // When the player is not ready yet, add the event to a queue
  // Each frame_id is associated with an own queue.
  // Each queue has three possible states:
  //  undefined = uninitialised / array = queue / 0 = ready
  if (!callPlayer.queue) callPlayer.queue = {};
  var queue = callPlayer.queue[frame_id],
    domReady = document.readyState == 'complete';

  if (domReady && !iframe) {
    // DOM is ready and iframe does not exist. Log a message
    window.console && console.log('callPlayer: Frame not found; id=' + frame_id);
    if (queue) clearInterval(queue.poller);
  } else if (func === 'listening') {
    // Sending the "listener" message to the frame, to request status updates
    if (iframe && iframe.contentWindow) {
      func = '{"event":"listening","id":' + JSON.stringify(''+frame_id) + '}';
      iframe.contentWindow.postMessage(func, '*');
    }
  } else if (!domReady || iframe && (!iframe.contentWindow || queue && !queue.ready) || (!queue || !queue.ready) && typeof func === 'function') {
    if (!queue) queue = callPlayer.queue[frame_id] = [];
    queue.push([func, args]);
    if (!('poller' in queue)) {
      // keep polling until the document and frame is ready
      queue.poller = setInterval(function() {
        callPlayer(frame_id, 'listening');
      }, 250);
      // Add a global "message" event listener, to catch status updates:
      messageEvent(1, function runOnceReady(e) {
        if (!iframe) {
          iframe = document.getElementById(frame_id);
          if (!iframe) return;
          if (iframe.tagName.toUpperCase() != 'IFRAME') {
            iframe = iframe.getElementsByTagName('iframe')[0];
            if (!iframe) return;
          }
        }
        if (e.source === iframe.contentWindow) {
          // Assume that the player is ready if we receive a
          // message from the iframe
          clearInterval(queue.poller);
          queue.ready = true;
          messageEvent(0, runOnceReady);
          setTimeout(function(){
            callPlayer(frame_id, 'playVideo');
          }, 500);
        }
      }, false);
    }
  } else if (iframe && iframe.contentWindow) {
    // When a function is supplied, just call it (like "onYouTubePlayerReady")
    if (func.call) return func();
    // Frame exists, send message
    iframe.contentWindow.postMessage(JSON.stringify({
      "event": "command",
      "func": func,
      "args": args || [],
      "id": frame_id
    }), "*");
  }
  /* IE8 does not support addEventListener... */
  function messageEvent(add, listener) {
    var w3 = add ? window.addEventListener : window.removeEventListener;
    w3 ?
      w3('message', listener, !1)
    :
      (add ? window.attachEvent : window.detachEvent)('onmessage', listener);
  }
}

function play_vdo(id, callback) {
  playing_vdo = id;

  callPlayer(id, function(){
    callPlayer(id, 'playVideo');
  });

  callback && callback();
}

function stop_vdo(id) {
  callPlayer(id, 'stopVideo');
}

function delegateMousewheel(e){
  var evt = window.event || e;
  var delta = evt.detail? evt.detail*(-120) : evt.wheelDelta;

  if ( delta<0 ){
    currentStep--;

    if (currentStep<0) {
      currentStep = 0;
      return;
    }
  }
  else {
    currentStep++;

    if (currentStep>maxStep) {
      currentStep = maxStep;
      return;
    }
  }
  
  setAngleStep(currentStep);
}

function updateClockTime(__step) {
  var __ii = (__step % 60).toString();
  var __hh = (Math.floor(__step / 60)).toString();
  $('#select-time').val(__hh.padLeft(2) + ':' + __ii.padLeft(2));
  $('#select-time_hidden').val(__hh.padLeft(2) + __ii.padLeft(2));
}

function addRandomColor() {
  var __color = [ 
    '#211142', '#382361', '#582e9a', '#7c8bdd', '#828eb2', '#86aaac', '#6b9294', '#d58fc0', '#ba9fc9',
    '#211142', '#382361', '#582e9a', '#7c8bdd', '#828eb2', '#86aaac', '#6b9294', '#d58fc0', '#ba9fc9',
    '#211142', '#382361', '#582e9a', '#7c8bdd', '#828eb2', '#86aaac', '#6b9294', '#d58fc0', '#ba9fc9',
    '#211142', '#382361', '#582e9a', '#7c8bdd', '#828eb2', '#86aaac', '#6b9294', '#d58fc0', '#ba9fc9' 
  ];
  $('.random-color').length > 0 && $('.random-color').each(function(){
    $(this).css('background-color', __color[ Math.round( Math.random() * (__color.length - 1) ) ] );
  });
}

function addScrollbar() {
  if (mobile_ui) 
    return;

  var visibleCard = false;
  var disableMove = false;

  if ($('#custom-scroll').length > 0) {
    var customScroll = $("#custom-scroll").tinyscrollbar({ 
      trackSize: 225, 
      thumbSize: 85 
    });

    $('#custom-scroll').find('.card-viewport').each(function(i) {
      $(this).data('pos', $(this).position().top);
    });

    var viewportHeight = $('#custom-scroll').height();
    var cardViewport = $('#custom-scroll').find('.card-viewport');
    var currentViewportPos = 0;

    customScroll.bind('move', function(e) {
      if (disableMove) {
        return;
      }

      currentViewportPos = -customScroll.find('.overview').position().top;
      visibleCard = cardViewport.filter(function(){
        return !$(this).hasClass('animated') && $(this).data('pos') >= currentViewportPos && $(this).data('pos') <= currentViewportPos + viewportHeight;
      });

      if (visibleCard.length<=0){
        disableMove = cardViewport.length == cardViewport.filter('.animated').length;
        return;
      }

      animateCard(visibleCard);
    });

    setTimeout(function() {
      customScroll.trigger('move');
    }, 1200);
  }
  
  if ($('#full-scroll').length > 0) {
    var fullScroll = $("#full-scroll").tinyscrollbar({ thumbSize: 85 });

    $('#full-scroll').find('.winner-on-date').each(function(i) {
      $(this).data('pos', $(this).position().top);
    });

    var viewportHeight = $('#full-scroll').height();
    var cardViewport = $('#full-scroll').find('.winner-on-date');
    var currentViewportPos = 0;

    fullScroll.bind('move', function() {
      if (disableMove) {
        return;
      }

      currentViewportPos = -fullScroll.find('.overview').position().top;
      visibleCard = cardViewport.filter(function(){
        return !$(this).hasClass('animated') && $(this).data('pos') >= currentViewportPos && $(this).data('pos') <= currentViewportPos + viewportHeight;
      });

      if (visibleCard.length<=0){
        disableMove = cardViewport.length == cardViewport.filter('.animated').length;
        return;
      }

      animateWinner(visibleCard);
    });

    fullScroll.trigger('move');
  }
}

function animateWinner(elems) {
  setTimeout(function() {
    elems.each(function(i) {
      $(this).velocity({ opacity: 1 }, {
        delay: 300 * i
      });

      $(this).find('.member-list li').each(function(j) {
        $(this).velocity({ marginLeft: 0, opacity: 1 }, {
          delay: (300 * i) + (100 * j)
        });
      });
    });
  }, 600);
}

function animateCard(elems) {
  var _card = elems.find('.card'),
    _time = elems.find('.time'),
    _date = elems.find('.date')
    ;

  setTimeout(function(){
    elems.each(function(i) {
      $(this).find('.time, .date').velocity({ translateY: 0, opacity: 1 }, {
        duration: 1000,
        delay: 300 * i
      });
    });

    _card.each(function(i) {
      $(this)
        .velocity({ translateY: 0, translateX: 0, opacity: 1 }, {
          delay: 150 * i
        });
    });
  }, 600);

  elems
    .addClass('animated');
}

function init_default() {
  if (mobile_ui) 
    return;

  $('.logo-scb').css({ top: '-100%', opacity: 0 });

  $('.nav-top a, .app-footer').css({ opacity: 0 });
}

function init_animate() {
  if (mobile_ui) 
    return;

  $('.logo-scb')
    .velocity({ top: '0%', opacity: 1 }, {
      duration: 1000,
      easing: [600, 20]
    });

  $('.app-footer')
    .velocity({ opacity: 1 }, { duration: 300 });

  $('.nav-top a').each(function(i) {
    $(this).velocity({ opacity: 1 }, { duration: 600, delay: (100 * i) });
  });
}

function init_home() {
    if (mobile_ui)
        return;

    setTimeout(function () {
        var _playerLi = $('.player-list li'),
          _player = $('.player-list .player'),
          _delay = 600;

        _playerLi.each(function (i) {
            $(this).velocity({ opacity: 1 }, {
                delay: _delay / 2 * i
            });
        });

        _player.each(function (i) {
            var _circle = $(this).parent().find('.svg');
            var _circleScreen = $(this).parent().find('.screen-circle');

            $(this)
              .velocity({ marginTop: 0, opacity: 1 }, {
                  easing: 'spring',
                  duration: 1000,
                  delay: _delay / 2 * i
              });

            $(this)
              .parent().find('.shadow')
              .velocity({ marginTop: '10%', opacity: 1 }, {
                  easing: 'spring',
                  duration: 1000,
                  delay: (_delay / 3 * i) + _delay
              });

            $(this)
              .parent().find('.time')
              .velocity({ marginTop: '0%', opacity: 1 }, {
                  delay: (_delay / 4 * i) + _delay,
                  complete: function () {
                      renderSmallClock(
                        _circle.attr('id'),
                        220,
                        220,
                        parseInt(_circle.attr('data-start')),
                        parseInt(_circle.attr('data-end')),
                        parseInt(_circle.attr('data-direction'))
                      );

                      _circleScreen.addClass('active');
                  }
              });
        });

        play_vdo('main-vdo');
    }, 600);
}

function init_join() {
  if (mobile_ui) 
    return;

  console.log('init join');
}

function init_howto() {
  if (mobile_ui) 
    return;

  console.log('init howto');
}

function init_gallery() {
  if (mobile_ui) 
    return;

  $('.vdo-list .vdo').each(function(i) {
    $(this).velocity({ opacity: 1 }, {
      delay: 150 * i
    })
  });
}

function init_winner() {
  if (mobile_ui) 
    return;
}

function init_archive() {
  var select_time = $('.select-time-range');
  select_time.find('.datepicker')
    .pickadate({
        min: [2014, 8, 1],
        max: true,
        format: 'dd mmm',
        formatSubmit: 'ddmm',
        onSet: function (context) {
          // redirect to ...
          // console.log( context.select );
            $('.button-select-date').html(select_time.find('.datepicker').val());

            var $frm = $('#frm-archive');
            $frm.find('input[name="date"]').val(select_time.find('#select-date_hidden').val());
            $frm.submit();
        }
    })
    ;

  select_time.find('.timepicker')
    .pickatime({
        format: 'HH:i',
        formatSubmit: 'HHi',
        onSet: function (context) {
          //redirect to ...
          // context.select
          // select_time.find('.timepicker').val()
            var $frm = $('#frm-archive');
            $frm.find('input[name="time"]').val(select_time.find('#select-time_hidden').val());
            $frm.submit();
        }
    });
}

// event register 
// ---
$('body')
  .delegate('.vex-close', 'click', function(e) {
    e.preventDefault();
    vex.close();
  })
  .delegate('.button-menu-toggle', 'click', function(e) {
    toggleMenu();
  });

$(window).load(function() {
  addScrollbar();
  addRandomColor();
});

$(document).ready(function() {
  var appBody = $('.app-body'),
    appNamespace = 'init_' + appBody.attr('id')
    ;

  init_default();

  Pace
    .on('done', function() {
      $('.contain-wrapper').addClass('active');

      init_animate();

      window[appNamespace].call(this);
    });
});

// --- test function ---

function test_open_player() {
  open_player('//www.youtube.com/embed/hXI9uqh-hNA?rel=0', '10:35');
}

function test_open_winner() {
  var winnerList = [
    { 
      'avatar': 'https://trello-avatars.s3.amazonaws.com/6f1a9434923c4a5b7a2da7ae57fa9f87/170.png',
      'name': 'Noppawitch Thienmongkol'
    },
    { 
      'avatar': 'https://trello-avatars.s3.amazonaws.com/6f1a9434923c4a5b7a2da7ae57fa9f87/170.png',
      'name': 'Noppawitch Thienmongkol'
    },
    { 
      'avatar': 'https://trello-avatars.s3.amazonaws.com/6f1a9434923c4a5b7a2da7ae57fa9f87/170.png',
      'name': 'Noppawitch Thienmongkol'
    }
  ];
  open_winner('//www.youtube.com/embed/hXI9uqh-hNA?rel=0', '10:35', '1 Aug', winnerList);
}

function test_open_selectdate() {
  open_selectDate();
}

function test_open_confirm() {
  open_confirm('https://trello-avatars.s3.amazonaws.com/6f1a9434923c4a5b7a2da7ae57fa9f87/170.png', '19:50');
}

function test_open_thankyou() {
  open_thankyou();
}

function test_open_register() {
  open_register();
}

function test_open_invite() {
  open_invite();
}

function test_open_login_facebook() {
  open_login_facebook();
}

function test_error_time() {
  error_time();
}
